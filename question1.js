const req = {
  cost_price: 32.67,
  sell_price: 45.0,
  inventory: 1200,
};
const calculateProfit = (profitReq) => {
  const { cost_price, sell_price, inventory } = profitReq;
  if (cost_price && sell_price && inventory) {
    const totalCostPrice = cost_price * inventory;
    const totalSellPrice = sell_price * inventory;
    const profit = Math.abs(totalSellPrice - totalCostPrice);
    return profit;
  } else {
    throw new Error("not found cost price or sell price or inventory.");
  }
};

calculateProfit(req);
